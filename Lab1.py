#!/usr/bin/env python
# coding: utf-8

# In[1]:


def add_numbers(num1, num2):
    return num1 + num2

if __name__ == "__main__":
    # Input from the user
    num1 = float(input("Enter the first number: "))
    num2 = float(input("Enter the second number: "))
    
    # Calculate the sum
    result = add_numbers(num1, num2)
    
    # Print the result
    print(f"The sum of {num1} and {num2} is {result}")


# In[ ]:




